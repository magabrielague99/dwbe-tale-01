class Perro{
    constructor(nombre, raza, color, estadoAdop){
        this.nombre=nombre;
        this.raza=raza;
        this.color=color;
        this.estadoAdop=estadoAdop;
    }

    obtenerEstadoAdopcion(){
        return this.estadoAdop;
    }

    modificarEstadoAdopcion(newEstado){
        switch(newEstado){
            case "1":
                this.estadoAdop="Adoptado";
            break;
            case "2":
                this.estadoAdop="En adopcion";
            break;
            case "3":
                this.estadoAdop="No adoptado";
            break;
            default:
            alert("opcion de estado de adopcion no valida");
        
        }
    }
        
    
}

let perros = [];
while(window.confirm("desea agregar otro Perro?")){
    let perro =  new Perro(
                        prompt("Nombre del perro"),
                        prompt("Raza del perro"),
                        prompt("Color del perro"));
    perro.modificarEstadoAdopcion(prompt("Presione 1 para Adoptado 2. para En adopcion 3 No adoptado"));
    perros.push(perro);
};

console.log("imprimir todos los perros");
perros.forEach(i => {
    console.log(i.nombre, i.raza, i.color, i.estadoAdop);
});

console.log("imprimir perros en adopcion");
const enAdopcion = perros.filter(perro => perro.obtenerEstadoAdopcion() === "En adopcion");
enAdopcion.forEach(i => {
    console.log(i.nombre, i.raza, i.color, i.estadoAdop);
});

console.log("mprimir perros adoptados");
const Adoptado = perros.filter(perro => perro.obtenerEstadoAdopcion() === "Adoptado");
Adoptado.forEach(i => {
    console.log(i.nombre, i.raza, i.color, i.estadoAdop);
});


console.log("mprimir perros No adoptados");
const noAdoptado = perros.filter(perro => perro.obtenerEstadoAdopcion() === "No adoptado");
noAdoptado.forEach(i => {
    console.log(i.nombre, i.raza, i.color, i.estadoAdop);
});

