class estudiante{
   constructor(nombre, codigo, curso){
        this.nombre=nombre;
        this.codigo=codigo;
        this.curso=curso;
    }

    getInformacion(){
        console.log('Mi nombre es ' + this.nombre);
        console.log('Mi codigo es' + this.codigo);
        console.log('Estoy en el curso de ' + this.curso);
        let mensaje=("Soy "+ this.nombre+", Mi codigo es " +this.codigo+", Estoy en el curso "+ this.curso);
        return mensaje;
    }
}

estudiante1= new estudiante('carlos', '1234', 'Back')

console.log(estudiante1.getInformacion());

document.getElementById("Estudiante").innerHTML= estudiante1.getInformacion();