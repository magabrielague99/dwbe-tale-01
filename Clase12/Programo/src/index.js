const express= require("express");
const app=express();
const {imprimirReques, imprimirPath}= require("../middlewares/imprimir.middelware")

//sin el next la applicacion se bloquea
//orden y posicion afecta, al get siguiente
//si esta arriba se de los gets afecta a todos

app.use(imprimirReques);
app.use(imprimirPath);

app.get("/ejemplo", (req,res)=>{
    res.json("respuesta ejemplo desde edpoint")
})

//que el middelware solo afecte una ruta
app.get("/ejemplo2", imprimirPath,(req,res)=>{
    res.json("respuesta ejemplo2 desde edpoint")
})

app.listen(3001, ()=>{console.log(("escuchando desde perto 3000"));})