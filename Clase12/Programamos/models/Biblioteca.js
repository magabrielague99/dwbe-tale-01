const Autores=[{
    id: 1,
    nombre: "jorge Luis",
    apellido:"Borges",
    fechaDeNacimiento: "24/08/1899",
    libros:[{
        id: 1,
        titulo:"Ficciones",
        descripcion:"Se trata de uno de sus mas...",
        anioPublicacion:1994
    },
    {
        id:2, 
        titulo: "El Aleph",
        descripcion: "Otra recopilacion de cuentos...",
        anioPublicacion: 1949
    }]},
    {
    id: 2,
    nombre: "Gabriel",
    apellido:"Garcia Marquez",
    fechaDeNacimiento: "06/03/1927",
    libros:[{
        id: 1,
        titulo:"El coronel no tiene quien le escriba",
        descripcion:"novela sobre...",
        anioPublicacion:1961
    },
    {
        id:2, 
        titulo: "los funerales de la mama grande",
        descripcion: "Otra recopilacion de cuentos...",
        anioPublicacion: 1962
    }]
}];

const obtenerAutores=()=>{
    return Autores;
}

const crearAutor=(nombre, apellido, fechaDeNacimiento, libros)=>{
    const id= Autores.length+1;
    const Autor=[{
        id: id,
        nombre: nombre,
        apellido: apellido,
        fechaDeNacimiento:fechaDeNacimiento,
        libros: libros
    }]
    Autores.push(Autor);
    return true;
}

const obtenerAutorId=(id)=>{
    const Autor=Autores.filter(Autor=>Autor.id==id);
    return Autor;
}

const eliminarAutorId=(id)=>{
    const index= Autores.findIndex((element) => element.id ==id)
    Autores.splice(index,1);
    return true;
}

const modificarAutor=(id, autorModificado)=>{
    const index= Autores.findIndex((element) => element.id ==id);
    Autores.splice(index, 1, autorModificado);
    return Autores;
}

const ObtenerLibros=(id)=>{
    const index= Autores.findIndex((element) => element.id ==id);
    return Autores[index].libros;
}

const agrearLibro=(id, nuevoLibro)=>{
    const index=Autores.findIndex((element=>element.id==id));
    Autores[index].libros.push(nuevoLibro);
    return true;
}

const obtenerLibroId=(idAutor, idLibro)=>{
    const Autor= Autores.findIndex((element=>element.id==idAutor));
    const libro= Autores[Autor].libros.findIndex(element=>element.id==idLibro);
    return Autores[Autor].libros[libro];
}

const modificarLibroId=(idAutor, idLibro, modifLibro)=>{
    const autor= Autores.findIndex((a=>a.id==idAutor));
    const libro= Autores[autor].libros.findIndex(l=>l.id==idLibro);
    Autores[autor].libros.splice(libro, 1, modifLibro);
    return true;
}

const elimiarLibroId=(idAutor, idLibro)=>{
    const autor= Autores.findIndex((a=>a.id==idAutor));
    const libro= Autores[autor].libros.findIndex(l=>l.id==idLibro);
    Autores[autor].libros.splice(libro, 1);
    return true;
}

module.exports={obtenerAutores, crearAutor, obtenerAutorId, eliminarAutorId, modificarAutor, ObtenerLibros, agrearLibro,
                obtenerLibroId, modificarLibroId, elimiarLibroId};