const {obtenerAutores, ObtenerLibros} = require('../models/Biblioteca');

function verificarAutor(req, res, next) {
    const {id}  = req.query; 
    const Autor = obtenerAutores().find(u => u.id == id);
    if (Autor){
        next();
    } 
    else {
        res.json('El autor no existe en nuestra lista');
    }
};

function verificarLibroAutor(req, res, next){
    const {id, idLibro}= req.query;
    console.log(id, idLibro);
    const autor= obtenerAutores().find(u=>u.id== id );
    const libro = ObtenerLibros(id).find(l=>l.id==idLibro);
    if(autor && libro) next();
    else res.json('El autor y/o libro no enconttrad');
};

module.exports = {verificarAutor, verificarLibroAutor};

