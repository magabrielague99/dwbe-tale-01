const express = require('express');
const router = express.Router();
const {verificarAutor, verificarAutorLibro, verificarLibroAutor}=require('../middlewares/verfAutorMiddleware');
const { obtenerAutores, crearAutor, obtenerAutorId, eliminarAutorId, modificarAutor, ObtenerLibros, agrearLibro,
        obtenerLibroId, modificarLibroId, elimiarLibroId} = require('../models/Biblioteca');

router.get('/', (req, res) => {
    res.json(obtenerAutores());
})

router.post('/', (req,res)=>{
    const {nombre, apellido, fechaDeNacimiento, libros}=req.body
    res.json(crearAutor(nombre, apellido, fechaDeNacimiento, libros));
})

router.use(verificarAutor);

router.get('/id', (req, res) => {
    const {id}=req.query
    res.json(obtenerAutorId(id));
})

router.delete('/id', (req,res)=>{
    const {id}=req.query;
    res.json(eliminarAutorId(id))
})

router.put('/id', (req, res)=>{
    const {id}=req.query
    const {autor}=req.body;
    res.json(modificarAutor(id, autor))
})

router.get('/id/libros', (req, res)=>{
    const {id}=req.query;
    res.json(ObtenerLibros(id));
})

router.put('/id/libros', (req, res)=>{
    const {id}=req.query;
    const {libro}=req.body;
    res.json(agrearLibro(id, libro));
})

router.use(verificarLibroAutor);

router.get('/id/libros/idLibro', (req, res)=>{
    const {id, idLibro}= req.query;
    res.json(obtenerLibroId(id, idLibro));
})

router.put('/id/libros/idLibro', (req, res)=>{
    const {id, idLibro}= req.query;
    const {libro} = req.body;
    res.json(modificarLibroId(id, idLibro, libro));
})

router.delete('/id/libros/idLibro', (req, res)=>{
    const {id, idLibro}= req.query;
    res.json(elimiarLibroId(id, idLibro));
})

module.exports = router;
