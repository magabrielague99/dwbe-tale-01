const express= require("express");
const app=express();
const autoresRoutes=require('../routes/autores.rout')

app.use(express.json());

app.use('/autores', autoresRoutes);

app.listen(3000, () => { console.log('Escuchando en el puerto 3000') });

