const express = require('express');
const app = express();
const { obtenerTelefonos, obtenerMitadTelefonos, obtenerTelefonoMayorPRecio, obtenerTelefonoMenorPrecio, ObtenerAgrupacionGama } = require('./models/Telefonos');

app.get('/telefonos', (req, res) => {
    res.json(obtenerTelefonos());
});

app.get('/mitadtelefonos', (req, res) => {
    res.json(obtenerMitadTelefonos());
});

app.get('/mayorprecio', (req, res) => {
    res.json(obtenerTelefonoMayorPRecio());
});

app.get('/menorprecio', (req, res) => {
    res.json(obtenerTelefonoMenorPrecio());
});

app.get('/gama', (req, res)=>{
    res.json(ObtenerAgrupacionGama())
})

app.listen(3000, () => {
    console.log('Escuchando en el puerto 3000');
});
