const telefonos = [
    {
        marca: "Samsung",
        gama: "Alta",
        modelo: "S11",
        pantalla: "19:9",
        sistema_operativo: "Android",
        precio: 1200
    },
    {
        marca: "Iphone",
        modelo: "12 pro",
        gama: "Alta",
        pantalla: "OLED",
        sistema_operativo: "iOs",
        precio: 1500
    },
    {
        marca: "Xiaomi",
        modelo: "Note 10s",
        gama: "Media",
        pantalla: "OLED",
        sistema_operativo: "Android",
        precio: 300
    },
    {
        marca: "LG",
        modelo: "LG el que sea",
        gama: "Alta",
        pantalla: "OLED",
        sistema_operativo: "Android",
        precio: 800
    }
];

const obtenerTelefonos = () => {
    return telefonos;
}

const obtenerMitadTelefonos = () => {
    const mitad = Math.round(telefonos.length / 2);
    const mitadTelefonos = [];
    for (let index = 0; index < mitad; index++) {
        const telefono = telefonos[index];
        mitadTelefonos.push(telefono);
    }
    return mitadTelefonos;
}

const obtenerTelefonoMayorPRecio = () => {
    let mayorPrecio=0;
    telefonos.forEach(tel => {
        if(tel.precio>mayorPrecio){
            mayorPrecio=tel.precio;
        }
    });
    return mayorPrecio;
}

const obtenerTelefonoMenorPrecio = () => {
    // organizarla con sort 
    let menorPrecio=99999999999;
    telefonos.forEach(tel => {
        if(tel.precio<menorPrecio){
            menorPrecio=tel.precio;
        }
    });
    return menorPrecio;
}



const ObtenerAgrupacionGama=()=>{
    /*const grouped = _.mapValues(_.groupBy(telefonos, 'gama'),
                          clist => clist.map(telefono => _.omit(telefono, 'gama')));
    return grouped*/
    /*const baja = telefonos.filter(telefono => telefono.gama=== "Baja");
    const media = telefonos.filter(telefono => telefono.gama=== "Media");
    const alta = telefonos.filter(telefono => telefono.gama=== "Alta");
    return [baja, media, alta];*/
}

module.exports = { obtenerTelefonos, obtenerMitadTelefonos, obtenerTelefonoMayorPRecio, obtenerTelefonoMenorPrecio, ObtenerAgrupacionGama};

