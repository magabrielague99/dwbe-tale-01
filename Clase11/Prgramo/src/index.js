require('dotenv').config();

const express = require('express');
const app =express();

const PORT=process.env.PORT || 3000;

app.get('/', (req, res) =>{
    //req(peticion)=peticion del cliente
    //res=respuesta
    res.send("hola mundo ");
})

/*
//acceder a una datos de otra acarpeta
app.use(express.static("../src/public"))
*/ 

//recibir informacion que se envia por body
app.use(express.json());
const listaUsuario=["ana", "juan", "camilo"];
// get obtener, pos crear, put modificar, delate borrar
//todas deben tenr un res porque sino se bloquea
app.get("/usuarios", (req,res)=>{
    res.json(listaUsuario);
})

//no se puede acceder diractamente del navegador
app.post("/usuarios", (req, res) =>{
    //obtener todo lo que haya en doby
    //const {parametro1, parametro2}= req.body;
    console.log(req.body.nombre);
    res.json(listaUsuario);
})

//actulizar usuarios
app.put("/usuarios", (req,res)=>{
    const {index}=req.query;
    const {nombre}=req.body;
    listaUsuario[index]=nombre;
    console.log(index, nombre);
    res.json("actulizando")
})

app.delete("/usuarios", (req,res)=>{
    const {index}=req.query;
    listaUsuario.splice(index,1);
    res.json("usuario borrado")
})

//activar al servidro en modo escucha
app.listen(PORT, ()=>{
    console.log("escuchando desde el puerto" +PORT);
})