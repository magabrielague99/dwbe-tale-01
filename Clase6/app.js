/* function funcionPrueba(nombre, apellido){
    console.log("Hola desde una funcion "  + nombre+ " " +apellido);
}

funcionPrueba("Gabriela", "Guerrero"); 
funcionPrueba("Miriam", "Pazmiño"); */

// var vs const  y let
/*no utilizar var porque se hace global en todo el documento
  Puede causar errores, Es mala practica 
*/

//const no cambian, una vez declarada permanece con el mismo valor
//Crear inicialmente todo como constate o anizar si cambia
const texto3="Hola desde constante js";

// variables que van a cambiar let 
// let crea variables de bloque o llaves
let texto2=" Hola  desde left js";
texto2="Redefinir";

if(true){
    let textoVarIf="Hola desde el if"
    console.log(textoVarIf);
}

/* var al estar dentro de una funcion solo exsite 
dentro de ella, de funcion 
function dentroDeFuncion(){
    var texto4="Hola texto 4"
}

console.log(texto4);
*/

//Funciones normal o regular
function saludar1(){
    console.log("hola 1");
}

//Funcion anonima
let saludar2 = function(){
    console.log("Hola 2");
}

//Arrow fuction
let saludar3 = ()=>{
    console.log("Hola 3");
}

saludar1();
saludar2();
saludar3();

document.getElementById('btn').addEventListener('click', saludar3);

function suma(n1, n2){
    console.log(n1+n2);
}

let suma2 =function(n1,n2){
    console.log(n1+n2);
}

let suma3 = (n1,n2)=>{
    console.log(n1,n2);
}

//solo si hay nuna linea dentro de los corchetes
let suma4=(n1,n2) => console.log(n1+n2);

let elDoble=(n1)=> {
    return n1*2;
}
 
// un solo parametro eliminar parentesis
// se puede quitar el return 
let elDoble2 = n1=> n1*2

suma(5,7);
suma2(1, 2);
suma3(5,2);
suma4(3,4)

console.log(elDoble(5));
console.log(elDoble2(5));