function holaClase(){
    return "hola acamica";
}

console.log(holaClase());

const mensaje =(texto) =>{
    // si no hay return se cologo undefine
    console.log(`el texto enviado es  ${texto}`);
    //tolowerCAse hace las letras minustulas
}

console.log(mensaje("hola de nuevo acamica"));

function hablar(texto){
    console.log(texto);
}
function hablarBajo(texto){
    console.log(texto.toLowerCase());
}

exports.hablar= hablar;
exports.hablarBajo =hablarBajo;

// o sino agregar export al inicio de la funcion 
// se recuperan en un array asi
// expor.modules={nombres de las funciones exportadas}


