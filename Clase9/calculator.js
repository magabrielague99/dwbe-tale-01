const fs = require('fs');

function sumar(num1, num2){
    op=num1+num2
    suma=` suma ${op} \n`;
    fs.appendFileSync('listaOperaciones.txt', suma, function (err) {
        if (err) console.log(err);
        else console.log('Saved!');
    });
    return suma;
}

function restar(num1, num2){
    op=num1-num2
    resta=` resta ${op} \n`;
    fs.appendFileSync('listaOperaciones.txt', resta, function (err) {
        if (err) console.log(err);
        else console.log('Saved!');
    });
    return resta;
}

function multiplicar(num1, num2){
    op=num1*num2
    multiplicacion=` multiplicacion ${op} \n`;
    fs.appendFileSync('listaOperaciones.txt', multiplicacion, function (err) {
        if (err) console.log(err);
        else console.log('Saved!');
    });
    return multiplicacion;
}

function dividir(num1, num2){
    op=num1/num2
    division=` divison ${op} \n`;
    fs.appendFileSync('listaOperaciones.txt', division, function (err) {
        if (err) console.log(err);
        else console.log('Saved!');
    });
    return division;
}

exports.sumar=sumar;
exports.restar=restar;
exports.multiplicar=multiplicar;
exports.dividir=dividir;