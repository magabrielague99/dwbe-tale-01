const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');
const express = require('express');
const app = express();


const swaggerOptions = {
    swaggerDefinition: {
      openapi: '3.0.0',
      info: {
        title: 'Challegne API',
        version: '1.0.0',
        descripcion:'proyecto 1 acamica backend'
      },
      servers:[{
          url:"http//localhost:5000",
          descripcion:"local server"
      }]
    },
    //Ubicacon de las apis 
    apis: ['./src/models/*.js']
  }

  
const swaggerDocs = swaggerJsDoc(swaggerOptions)

app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerDocs));

app.listen(5000, () => console.log("listening on 5000"));

  
