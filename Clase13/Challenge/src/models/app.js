const express = require('express');
const app = express();

/**
 * @swagger
 * /estudiantes:
 *  post:
 *    description: Crea un nuevo estudiante
 *    parameters:
 *    - name: nombre
 *      description: Nombre del estudiante
 *      in: formData
 *      required: true
 *      type: string
  *    - name: edad
 *      description: Edad del estudiante
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 * 
 */
app.post('/estudiantes', (req, res) => {
    res.status(201).send();
});

/**
 * @swagger
 * /email
 */
router.put('/:email', (req, res) => {
    console.log(req.body);
    res.json(usuario);
});


app.listen(5000, () => console.log("listening on 5000"))
