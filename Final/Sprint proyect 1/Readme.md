# MI PRIMER API.
## Recursos
- Node.js
- Express.js
- Dotenv
- Swagger
- Express Basic Auth

## Instalación
#### 1. Clonar el proyecto
```
git clone https://gitlab.com/DDVS/deliah-resto-api.git
```
#### 2. Instalar dependencias
```
npm install
```
#### 3. Correr servidor
```
npm run start
```
```
## Documentation 
[Documentation](http://localhost:3000/api-docs) Se puede acceder en http://localhost:3000/api-docs

Usuarios:
| Usuario        | Contraseña  ||Rol          |
|----------------|-------------||-------------|
| ggmaria        | 12345       ||Administrador|
| pepe2          | 1234A       ||Usuario      |

**\*Inicie sesión en la documentación de swagger con nombre de usuario y contraseña*