const { obtenerUsuarios } = require('../models/usuario.model');

const esAdmin=(req, res, next)=>{
    const nombreUsuario= req.auth.user;
    const usuarioEncontrado = obtenerUsuarios().findIndex(u => u.nombreUsuario === nombreUsuario);
    const esAdmin= obtenerUsuarios()[usuarioEncontrado].esAdmin;
    if(esAdmin) next();
    else {
        console.log("Error: 403, Ruta solo para administradores");
        res.status(403).json("Ruta solo para administradores")
    };
};

module.exports = esAdmin;