const { obtenerPedidos } = require('../models/pedidos.models');

const estaPediente=(req, res, next)=>{
    const {idPedido}= req.params;
    const indicePedido = obtenerPedidos().findIndex(u => u.idPedido == idPedido);
    const estadoPendiente= obtenerPedidos()[indicePedido].estadoPedido === "pendiente";
    if(estadoPendiente) next();
    else {
        console.log("Error: 400, El pedido ha sido confirmado, no se puede modificar");
        res.status(400).json("El pedido ha sido confirmado, no se puede modificar");
    }
};

module.exports= estaPediente;