const { obtenerUsuarios } = require('../models/usuario.model');

const autenticacion = (nombreUsuario, contrasenia) => {
    const usuarioEncontrado = obtenerUsuarios().find(u => u.nombreUsuario === nombreUsuario && u.contrasenia === contrasenia);
    if (usuarioEncontrado) return true;
    else return false;
}

module.exports = autenticacion;
