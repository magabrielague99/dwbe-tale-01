const express = require('express');
const router = express.Router();
const {agregarUsuario, IniciarSesion}=require('../models/usuario.model');
const {obtenerProductos}=require('../models/producto.model');

/**
 * @swagger
 * /login/registro:
 *  post:
 *      summary: Crea un usuario en el sistema
 *      security: []
 *      tags: [Login]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/registro'
 *      responses:
 *          201:
 *              description: Usuario creado
 *          400:
 *              descriptcion: Peticion invalida
 */
router.post('/Registro', (req, res) => {
    const {usuario}= req.body
    const [codigo, mensaje]= agregarUsuario(usuario);
    console.log(codigo, mensaje);
    res.status(codigo).json(mensaje);
})

/**
 * @swagger
 * /Login:
 *  post:
 *      summary: Verifica si el usuario logeo correctamente
 *      tags: [Login]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/login'
 *      responses:
 *          200:
 *              description: el usuario logeado correctamete
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/login'
 *          401:
 *              description: Usuario aun no ha iniciado sesion
 */
router.post('/Login', (req,res)=>{
    const {usuario}= req.body;
    const [codigo, mensaje]= IniciarSesion(usuario);
    console.log(codigo, mensaje);
    res.status(codigo).json(mensaje);
})

/**
 * @swagger
 * tags:
 *  name: Login
 *  description: Seccion de Registro de usuario
 * 
 * components: 
 *  schemas:
 *      registro:
 *          type: object
 *          required:
 *              -nombreUsuario
 *              -nombreCompleto
 *              -email
 *              -telefono
 *              -direccion
 *              -contrasena
 *          properties:
 *              nombreUsuario:
 *                  type: string
 *                  description: nombre de usuario 
 *              nombreCompleto:
 *                  type: string
 *                  description: nombre completo del usuario
 *              email:
 *                  type: string
 *                  description: Email del usuario
 *              telefono: 
 *                  type: integer
 *                  description: numero de telefono del usuario
 *              direccion:
 *                  type: string
 *                  description: direccion del usuaruio
 *              contrasena:
 *                  type: string
 *                  description: Contrasena del usuario
 *          example:  
 *              usuario: 
 *                  {
 *                      nombreUsuario: juan34,
 *                      nombreCompleto: juan camilo rosas,
 *                      email: juan34f@gmail.com,
 *                      telefono: 3170000000,    
 *                      direccion: calle 57 N° 8-97,
 *                      contrasenia: 12345a                                  
 *                  }  
 *      login:
 *          type: object
 *          required:
 *              -nombreUsuario
 *              -contrasenia
 *          properties:
 *              nombreUsuario:
 *                  type: string
 *                  description: nombre de usuario 
 *              contrasenia:
 *                  type: string
 *                  description: Contrasena del usuario
 *          example:  
 *              usuario: 
 *                  {
 *                      nombreUsuario: juan34,
 *                      contrasenia: 12345a                                  
 *                  }               
 */
module.exports= router;