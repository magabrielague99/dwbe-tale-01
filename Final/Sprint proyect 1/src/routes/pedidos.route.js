const express = require('express');
const router = express.Router();
const {agregarPedido, modificarContidadProducto, eliminarProductoPedido, agregarProductoPedido, obtenerPedidosUsuario,
        cambiarEstado, obtenerPedidos, cambiarEstadoUsuario}= require('../models/pedidos.models');
const estaPendiente = require('../middlewares/estaPediente.middlegare');
const  esAdmin= require('../middlewares/esAdmin.middleware');
const {obtenerProductos} = require('../models/producto.model');
const {obtenermediosPago} = require('../models/mediosPago.model');

/**
 * @swagger
 * /pedidos:
 *  get:
 *      summary: Obtener todos los pedidos realizados por el usuario
 *      tags: [Pedidos]
 *      responses:
 *          200:
 *              description: Lista de productos, lista de medios de pago y lista de pedidos realizados por el usuario
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/ObtenerPedido'
 *          401:
 *              description: Usuario aun no ha iniciado sesion
 */
router.get('/', (req, res)=>{
    const usuario= req.auth.user;   
    console.log([obtenerProductos(), obtenermediosPago(), obtenerPedidosUsuario(usuario)]);
    res.status(200).json([obtenerProductos(), obtenermediosPago(), obtenerPedidosUsuario(usuario)]);
})


/**
 * @swagger
 * /pedidos/obtenerPedidos:
 *  get:
 *      summary: Obtener todos los pedidos  en el sistema
 *      tags: [Pedidos]
 *      responses:
 *          200:
 *              description: Lista de pedidos del sistema
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/ObtenerPedido'
 *          203:
 *              description: Ruta solo para administrador
 *          401:
 *              description: Usuario aun no ha iniciado sesion
 */
router.get('/obtenerPedidos', esAdmin, (req, res)=>{
    console.log(ObtenerPedidos());
    res.json(obtenerPedidos());
})


/**
 * @swagger
 * /pedidos/agregarPedido:
 *  post:
 *      summary: Crea un pedido en el sistema
 *      tags: [Pedidos]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/AgregarPedido'
 *      responses:
 *          201:
 *              description: Producto creado
 *          400:
 *              description: campos vacios o prodcuto no encontrado o medio de pago no aceptado 
 *          401:
 *              description: Usuario aun no ha iniciado sesion
 */
router.post('/agregarPedido', (req, res)=>{
    const {pedido}= req.body;
    const nombreUsuario= req.auth.user;
    const [codigo, mensaje]= agregarPedido(pedido, nombreUsuario);
    cconsole.log(codigo, mensaje);
    res.status(codigo).json(mensaje);
})


/**
 * @swagger
 * /pedidos/modificarCantidadProducto/{idPedido}/{idProducto}:
 *  put:
 *      summary: Modifica la cantidad de producto en el pedido
 *      tags: [Pedidos]
 *      parameters:
 *          - in: path
 *            name: idPedido
 *            schema:
 *                $ref: '#/components/schemas/ModificarIdProducto'
 *            required: true
 *          - in: path
 *            name: idProducto
 *            schema:
 *                $ref: '#/components/schemas/ModificarIdProducto'
 *            required: true
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/ModificarCantidaProducto'
 *      responses:
 *          201:
 *              description: Producto creado
 *          400:
 *              description: campos vacios o prodcuto no encontrado o medio de pago no aceptado 
 *          401:
 *              description: Usuario aun no ha iniciado sesion 
 */
router.put('/modificarCantidadProducto/:idPedido/:idProducto',estaPendiente, (req, res) => {
    const {idPedido, idProducto}=req.params;
    const {cantidadProducto}= req.body;
    const [codigo, mensaje]= modificarContidadProducto(idPedido, idProducto, cantidadProducto);
    console.log(codigo, mensaje);
    res.status(codigo).json(mensaje);
})

/**
 * @swagger
 * /pedidos/modificarEliminarProductoPedido/{idPedido}/{idProducto}:
 *  delete:
 *      summary: elimina un producto de la lista en el pedido
 *      tags: [Pedidos]
 *      parameters:
 *          - in: path
 *            name: idPedido
 *            schema:
 *                $ref: '#/components/schemas/ModificarIDProdcuto'
 *            required: true
 *          - in: path
 *            name: idProducto
 *            schema:
 *                $ref: '#/components/schemas/ModificarIdProducto'
 *            required: true
 *      responses:
 *          200:
 *              description: medio de pago eliminado
 *          400:
 *              description: campos vacios o prodcuto no encontrado o medio de pago no aceptado 
 *          401:
 *              description: Usuario aun no ha iniciado sesion
 */
router.delete('/modificarEliminarProductoPedido/:idPedido/:idProducto',estaPendiente, (req, res)=>{
    const {idPedido, idProducto}= req.params;
    const [codigo, mensaje]= eliminarProductoPedido(idPedido, idProducto);
    console.log(codigo, mensaje);
    res.status(codigo).json(mensaje);
})

/**
 * @swagger
 * /pedidos/modificarAgregarProductoPedido/{idPedido}:
 *  post:
 *      summary: Modifica la cantidad de producto en el pedido
 *      tags: [Pedidos]
 *      parameters:
 *          - in: path
 *            name: idPedido
 *            schema:
 *                $ref: '#/components/schemas/ModificarIdPedido'
 *            required: true
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/modficarAgregarProductoPedido'
 *      responses:
 *          400:
 *              description: campos vacios o prodcuto no encontrado o medio de pago no aceptado 
 *          401:
 *              description: Usuario aun no ha iniciado sesion
 */
router.post('/modificarAgregarProductoPedido/:idPedido',estaPendiente,(req,res)=>{
    const {idPedido}= req.params;
    const {producto}= req.body;
    const [codigo, mensaje]= agregarProductoPedido(idPedido, producto);
    console.log(codigo, mensaje);
    res.status(codigo).json(mensaje);
})

/**
 * @swagger
 * /pedidos/modificarEstado/{idPedido}:
 *  put:
 *      summary: Modifica el estado del pedido
 *      tags: [Pedidos]
 *      parameters:
 *          - in: path
 *            name: idPedido
 *            schema:
 *                $ref: '#/components/schemas/ModificarIdPedido'
 *            required: true
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/modificarEstado'
 *      responses:
 *          201:
 *              description: Producto creado
 *          400:
 *              description: campos vacios o prodcuto no encontrado o medio de pago no aceptado 
 *          401:
 *              description: Usuario aun no ha iniciado sesion
 *          403:
 *              description: Ruta solo para administradores
 */
router.put('/modificarEstado/:idPedido', esAdmin, estaPendiente, (req, res)=>{
    const {idPedido}= req.params;
    const usuario = req.auth.user;
    const {estadoPedido}= req.body;
    const [codigo, mensaje]= cambiarEstado(idPedido, usuario, estadoPedido);
    console.log(codigo, mensaje);
    res.status(codigo).json(mensaje);
})

/**
 * @swagger
 * /pedidos/modificarEstadoUsuario/{idPedido}:
 *  put:
 *      summary: Modifica el estado del pedido
 *      tags: [Pedidos]
 *      parameters:
 *          - in: path
 *            name: idPedido
 *            schema:
 *                $ref: '#/components/schemas/ModificarIdPedido'
 *            required: true
 *      responses:
 *          201:
 *              description: Producto creado
 *          400:
 *              description: campos vacios o prodcuto no encontrado o medio de pago no aceptado 
 *          401:
 *              description: Usuario aun no ha iniciado sesion
 *          403:
 *              description: Ruta solo para administradores
 */
router.put('/modificarEstadoUsuario/:idPedido', estaPendiente, (req, res)=>{
    const {idPedido}= req.params;
    const usuario= req.auth.user;
    const [codigo, mensaje]= cambiarEstadoUsuario(idPedido, usuario);
    console.log(codigo, mensaje);
    res.status(codigo).json(mensaje);
})

/**
 * @swagger
 * tags:
 *  name: Pedidos
 *  description: Seccion de pedidos 
 * 
 * components: 
 *  schemas:
 *      AgregarPedido:
 *          type: object
 *          required:
 *              -Productos
 *              -estadoPedido
 *              -medioPago
 *          properties:
 *              Productos:
 *                  type: array
 *                  description: Lusta de productos de un usuario  
 *              estadoPedido:
 *                  type: string
 *                  description: estado del pedido
 *                  schema:
 *                      $ref: '#/components/schemas/estadoPedido'
 *              medioPago:
 *                  type: string
 *                  descrption: medio de pago del pedido
 *          example:  
 *              pedido: 
 *                  {
 *                      productos:
 *                          [
 *                              {
 *                                  "idProducto": 1,
 *                                  "cantidadProducto": 3
 *                              },
 *                              {
 *                                  "idProducto": 2,
 *                                  "cantidadProducto": 3
 *                              }
 *                          ],
 *                      estadoPedido: "pendiente",
 *                      medioPago: "Efectivo"                                 
 *                  }  
 *      estadopedido:
 *          type: string
 *          enum:
 *              - pedinte
 *              - completo
 *      ModficarIdPedido:
 *          type: object
 *          required:
 *              -idPedido
 *          properties:
 *              idPedido:
 *                  type: integer
 *                  description: Id del pedido
 *      ModficarIdProducto:
 *          type: object
 *          required:
 *              -idProducto
 *          properties:
 *              idProducto:
 *                  type: integer
 *                  description: Id del producto 
 *      ModificarCantidaProducto:
 *          type: object
 *          required:
 *              -cantidadProducto
 *          properties:
 *              cantidadProducto:
 *                  type: integer
 *                  description: cantidad de producto 
 *          example:  
 *              cantidadProducto: 4
 *      modficarAgregarProductoPedido:
 *          type: object
 *          required:
 *              -idProducto
 *              -cantidadProducto
 *          properties:
 *              idProducto:
 *                  type: integer
 *                  description: identificador del producto
 *              cantidadProducto:
 *                  type: integer
 *                  description: cantidad de producto 
 *          example:  
 *              producto: 
 *                  {
 *                      "idProducto": 3,
 *                      "cantidadProducto": 2
 *                  }    
 *      ObtenerPedido:
 *          type: object
 *          required:
 *              -IdProducto
 *              -nombreProducto
 *              -precio
 *              -IdMedioPago
 *              -nombreMedioPago
 *              -IdPedido
 *              -productos
 *              -totalCuenta
 *              -direccioin
 *              -medioPago
 *              -estadoPedido
 *          properties:
 *              IdPedido:
 *                  type: number
 *                  descrption: Numero de pedido
 *              productos:
 *                  type: array
 *                  description: Lista de productos de un usuario  
 *              direccion:
 *                  type: string
 *                  description: direccion de envio del pedido
 *              totalCuenta:
 *                  type: integer
 *                  description: Valor total de la cuenta
 *              estadoPedido:
 *                  type: string
 *                  description: estado del pedido
 *              medioPago:
 *                  type: string
 *                  descrption: medio de pago del pedido
 *          example:  
 *              Productos:
 *                  {
 *                      idProducto: 1,
 *                      nombrePdocuto: Hamburguesa,
 *                      precio: 8000
 *                  }
 *              mediosPago:
 *                  {
 *                      idMedioPago: 1,
 *                      nombreMedioPago: Efectivo
 *                  }
 *              pedido: 
 *                  {
 *                      "Numero de Pedido" : 1,
 *                      "Productos" : 
 *                          {
 *                              idProducto: 1,
 *                              cantidadProducto: 4
 *                          },
 *                      "Total  de la Cuenta" : 20000,
 *                      "Direccion" : calle 8 N° 5-93,
 *                      "Medio pago" : Efectivo,
 *                      "Estado pedido" : Confirmado                                 
 *                  }
 *      modificarEstado:
 *          type: object
 *          required:
 *              -estadoPedido
 *          properties:
 *              estadoPedido:
 *                  type: string
 *                  description: estado del producto 
 *          example:  
 *              estadoPedido: Enviado
 */
module.exports=router;