const express = require('express');
const router = express.Router();
const {agregarProducto, modificarProducto, eliminarProducto}= require('../models/producto.model');


/**
 * @swagger
 * /productos:
 *  post:
 *      summary: Crea un producto en el sistema
 *      tags: [Productos]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/Agregar'
 *      responses:
 *          201:
 *              description: Producto creado
 *          400:
 *              description: peticion no valida
 *          401:
 *              description: Usuario aun no ha iniciado sesion
 *          403: 
 *              description: Ruta solo para administradores
 */
router.post('/', (req, res) => {
    const {producto}= req.body;
    const [codigo, mensaje]= agregarProducto(producto);
    console.log(codigo, mensaje);
    res.status(codigo).json(mensaje);
})

/**
 * @swagger
 * /productos/{idProducto}:
 *  put:
 *      summary: Modifica un producto en el sistema
 *      tags: [Productos]
 *      parameters:
 *          - in: path
 *            name: idProducto
 *            schema:
 *                $ref: '#/components/schemas/Modificar'
 *            required: true
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/Agregar'
 *      responses:
 *          200:
 *              description: Producto modificado
 *          400:
 *              description: Peticion no valida
 *          401:
 *              description: Usuario aun no ha iniciado sesion
 *          403: 
 *              description: Ruta solo para administradores
 */
router.put('/:idProducto', (req, res) => {
    console.log(req.params);
    const {idProducto}=req.params;
    const {producto}= req.body;
    const [codigo, mensaje]= modificarProducto(idProducto, producto);
    console.log(codigo, mensaje);
    res.status(codigo).json(mensaje);
})

/**
 * @swagger
 * /productos/{idProducto}:
 *  delete:
 *      summary: elimina un producto en el sistema
 *      tags: [Productos]
 *      parameters:
 *          - in: path
 *            name: idProducto
 *            schema:
 *                 $ref: '#/components/schemas/Modificar'
 *            required: true
 *      responses:
 *          200:
 *              description: Producto eliminado
 *          400:
 *              description: Peticion no valida
 *          401:
 *              description: Usuario aun no ha iniciado sesion
 *          403: 
 *              description: Ruta solo para administradores
 */
router.delete('/:idProducto', (req, res)=>{
    console.log(req.params);
    const {idProducto}= req.params;
    const [codigo, mensaje]= eliminarProducto(idProducto);
    console.log(codigo, mensaje);
    res.status(codigo).json(mensaje);
})

/**
 * @swagger
 * tags:
 *  name: Productos
 *  description: Seccion de metodos de producto 
 * 
 * components: 
 *  schemas:
 *      Agregar:
 *          type: object
 *          required:
 *              -nombreProducto
 *              -precio
 *          properties:
 *              nombreProducto:
 *                  type: string
 *                  description: Nombre del producto  
 *              precio:
 *                  type: integer 
 *                  description: Precio del producto 
 *          example:  
 *              producto: 
 *                  {
 *                      nombreProducto: papas fritas,
 *                      precio: 4000,                                 
 *                  }  
 *      Modficar:
 *          type: object
 *          required:
 *              -idProducto
 *          properties:
 *              idProducto:
 *                  type: integer
 *                  description: Id del producto 
 *              
 */
module.exports=router;