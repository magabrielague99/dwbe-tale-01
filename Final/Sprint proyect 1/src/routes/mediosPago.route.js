const express = require('express');
const router = express.Router();
const {agregarMedioPago, modificarMedioPago, eliminarMedioPago, obtenermediosPago}= require('../models/mediosPago.model');

/**
 * @swagger
 * /mediosPago:
 *  get:
 *      summary: Obtener todos los medios de pago  del sistema
 *      tags: [mediosPago]
 *      responses:
 *          200:
 *              description: Lista de medios de pago del sistema
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/AgregarMedioPago'
 *          401:
 *              description: Usuario aun no ha iniciado sesion
 *          403: 
 *              description: Ruta solo para administradores
 */
router.get('/', (req, res)=>{
    console.log(obtenermediosPago());
    res.json(obtenermediosPago())
})

/**
 * @swagger
 * /mediosPago:
 *  post:
 *      summary: Crea un medio de pago en el sistema
 *      tags: [mediosPago]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/AgregarMedioPago'
 *      responses:
 *          201:
 *              description: medio de pago creado
 *          401:
 *              description: Usuario aun no ha iniciado sesion
 *          403: 
 *              description: Ruta solo para administradores
 */
router.post('/', (req, res) => {
    const {nombreMedioPago}= req.body;
    const [codigo, mensaje]= agregarMedioPago(nombreMedioPago);
    console.log(codigo, mensaje);
    res.status(codigo).json(mensaje);
})

/**
 * @swagger
 * /mediosPago/{idMedioPago}:
 *  put:
 *      summary: Modifica un medio de pago en el sistema
 *      tags: [mediosPago]
 *      parameters:
 *          - in: path
 *            name: idMedioPago
 *            schema:
 *                $ref: '#/components/schemas/ModificarMedioPago'
 *            required: true
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/AgregarMedioPago'
 *      responses:
 *          200:
 *              description: Nombre medio de pago Modificado
 *          400:
 *              description: Peticion Incorrecta
 *          401:
 *              description: Usuario aun no ha iniciado sesion
 *          403: 
 *              description: Ruta solo para administradores
 */
router.put('/:idMedioPago', (req, res) => {
    const {idMedioPago}=req.params;
    const {nombreMedioPago}= req.body;
    const [codigo, mensaje]= modificarMedioPago(idMedioPago, nombreMedioPago);
    console.log(codigo, mensaje);
    res.status(codigo).json(mensaje);
})

/**
 * @swagger
 * /mediosPago/{idMedioPago}:
 *  delete:
 *      summary: elimina un medio de pago en el sistema
 *      tags: [mediosPago]
 *      parameters:
 *          - in: path
 *            name: idMedioPago
 *            schema:
 *                 $ref: '#/components/schemas/ModificarMedioPago'
 *            required: true
 *      responses:
 *          200:
 *              description: medio de pago eliminado
 *          400:
 *              description: Peticion incorrecta
 *          401:
 *              description: Usuario aun no ha iniciado sesion
 *          403: 
 *              description: Ruta solo para administradores
 */
router.delete('/:idMedioPago', (req, res)=>{
    const {idMedioPago} = req.params;
    const [codigo, mensaje]= eliminarMedioPago(idMedioPago);
    console.log(codigo, mensaje);
    res.status(codigo).json(mensaje);
})

/**
 * @swagger
 * tags:
 *  name: mediosPago
 *  description: Seccion de metodos de medio de pago 
 * 
 * components: 
 *  schemas:
 *      AgregarMedioPago:
 *          type: object
 *          required:
 *              -nombreMedioPago
 *          properties:
 *              nombreMedioPago:
 *                  type: string
 *                  description: Nombre del medio de pago  
 *          example: 
 *              nombreMedioPago: "Cheque"
 *      ModficarMedioPago:
 *          type: object
 *          required:
 *              -idMedioPAgo
 *          properties:
 *              idMedioPago:
 *                  type: integer
 *                  description: Id del medio de pago 
 *              
 */
module.exports=router;