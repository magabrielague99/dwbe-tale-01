const express = require('express');
const basicAuth = require('express-basic-auth');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');

//importar funciones del programador
const loginRoutes = require('./routes/login.route');
const productoRoutes=require('./routes/producto.route');
const mediosPagoRoutes=require('./routes/mediosPago.route');
const pedidosRoutes= require('./routes/pedidos.route');
const swaggerOptions = require('./utils/swaggerOptions');

const app = express();
app.use(express.json());

const autenticacion = require('./middlewares/Autenticacion.middleware');
const esAdmin = require('./middlewares/esAdmin.middleware');
const swaggerSpecs = swaggerJsDoc(swaggerOptions);

app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerSpecs));

app.use(loginRoutes);

app.use(basicAuth({ authorizer: autenticacion }));

app.use('/pedidos', pedidosRoutes);


app.use(esAdmin);

app.use('/productos', productoRoutes);

app.use('/mediosPago', mediosPagoRoutes);

app.listen(3000, () => { console.log('Escuchando en el puerto 3000') });