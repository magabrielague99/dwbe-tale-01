const mediosPago =[{
    idMedioPago: 1,
    nombreMedioPago: "Efectivo",
},
{
    idMedioPago: 2,
    nombreMedioPago: "Tarjeta",
}
]

const obtenermediosPago=()=>{
    return mediosPago;
}

const agregarMedioPago = (nombreMedioPago) => {
    if(nombreMedioPago=="") return [400, "Nombre de medio de pago vacio "]; 
    else{
        const nombreMedioPagoDuplicado= mediosPago.find(u=>u.nombreMedioPago===nombreMedioPago);
        if(nombreMedioPagoDuplicado) return [400, "Medo de pago ya registrado"]; 
        else{
            MedioPago={
                idMedioPago: mediosPago[mediosPago.length-1].idMedioPago+1,
                nombreMedioPago: nombreMedioPago,
            }
            mediosPago.push(MedioPago);
            return [201, "Nuevo medio de pago agregado"];            
        }  
    }
        
}

const modificarMedioPago=(idMedioPago, nombreMedioPago)=>{
    if(Number.parseInt(idMedioPago)){
        const indiceIdMedioPago= mediosPago.findIndex(p=>p.idMedioPago===Number.parseInt(idMedioPago));
            if(indiceIdMedioPago===-1) return [400, "Medio de pago no encontrado en la plataforma"];
            else{
                if(nombreMedioPago=="") return [400, "campo medio de pago vacio"];
                else{
                    const nombreMedioPagoDuplicado= mediosPago.find(u=>u.nombreMedioPago===nombreMedioPago);
                    if(nombreMedioPagoDuplicado) return [400, "medio de pago ya registrado"];
                    else{
                        mediosPago[indiceIdMedioPago].nombreMedioPago= nombreMedioPago;
                        return [200, "Medio de modificado"];
                    }   
                }
            }
    }else   return[400, "Formato de Id no valido"];  
}

const eliminarMedioPago=(idMedioPago)=>{
    if(Number.parseInt(idMedioPago)){
        const indiceMedioPago=mediosPago.findIndex(p=>p.idMedioPago === Number.parseInt(idMedioPago));
        if(indiceMedioPago===-1) return [400, "Id no encontrado"];
        mediosPago.splice(indiceMedioPago,1);
        return [200, "medio de pago eliminado"];
    }else return[400, "Formato Id no valido "]
        
}

module.exports={agregarMedioPago, modificarMedioPago, obtenermediosPago, eliminarMedioPago}