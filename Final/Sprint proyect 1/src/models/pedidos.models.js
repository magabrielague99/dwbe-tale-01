const {obtenerProductos}= require('./producto.model');
const {obtenerUsuarios}= require('./usuario.model');
const {obtenermediosPago}= require('./mediosPago.model');

const pedidos =[
    {
        idPedido: 1,
        productos:[
            {
                idProducto: 1,
                cantidadProducto: 2

            },
            {
                idProducto: 2,
                cantidadProducto: 2
            }
        ],
        totalPagar: 26000,
        medioPago: "efectivo",
        direccion:  "calle 27 N° 7-58",
        usuario: {
            id: 1,
            nombreUsuario: "ggmaria",
            nombreCompleto: "Gabriela Guerrero",
            email:"gabrielaguerrero57@hotmail.com",
            telefono: 3186090430,            
        },
        estadoPedido: "pendiente"
    },
    {
        idPedido: 2,
        productos:[
            {
                idProducto: 2,
                cantidadProducto: 1

            },
            {
                idProducto: 3,
                cantidadProducto: 1
            }
        ],
        totalPagar: 6500,
        medioPago: "Tarjeta",
        direccion:  "calle 30 N° 8-50",
        usuario: {
            id: 2,
            nombreUsuario: "pepe2",
            nombreCompleto: "Pedro perez",
            email:"pepito@hotmail.com",
            telefono: 3167587865,            
        },
        estadoPedido: "pendiente"
    }
];

const obtenerPedidos=()=>{
    return pedidos;
}

const agregarPedido=(nuevoPedido, nombreUsuario)=>{
    const {productos, estadoPedido, medioPago}= nuevoPedido;
    if(productos === [] || estadoPedido ==="", medioPago==="") return "faltan campos";
    else{
        if(estadoPedido=="pendiente" || estadoPedido=="confirmado"){
            let [totalCuenta, productoExiste]=calcularTotalPagar(productos);
            if(productoExiste){
                const medioPagoRegistadro=obtenermediosPago().find(m=>m.nombreMedioPago==medioPago)
            if(medioPagoRegistadro){
                const indiceUsuario= obtenerUsuarios().findIndex(u=>u.nombreUsuario===nombreUsuario);
                pedido={
                    idPedido: pedidos[pedidos.length-1].idPedido+1,
                    productos: productos,
                    totalPagar: totalCuenta,
                    direccion: obtenerUsuarios()[indiceUsuario].direccion,
                    usuario:{
                        nombreUsuario: nombreUsuario,
                        nombreCompleto: obtenerUsuarios()[indiceUsuario].nombreCompleto,
                        email: obtenerUsuarios()[indiceUsuario].email,
                        telefono: obtenerUsuarios()[indiceUsuario].telefono,
                    },
                    estadoPedido: estadoPedido,
                }
                pedidos.push(pedido);
                return [201,"pedido Agregado"];
                } else return [400,"medio de pago no admitido"];
            }else  return [400, "producto no encontrado"]; 
        } else return [400, "estado del pedido no aceptado"];
    }
    
}

const modificarContidadProducto=(idPedido, idProducto, cantidadProducto)=>{
    const indicePedido= pedidos.findIndex(ped=>ped.idPedido==idPedido);
    if(indicePedido>-1){
        const indiceProducto= pedidos[indicePedido].productos.findIndex(prod=>prod.idProducto==idProducto);
        if(indiceProducto>-1){
            if(cantidadProducto != ""){
                pedidos[indicePedido].productos[indiceProducto].cantidadProducto=cantidadProducto;
                const productos= pedidos[indicePedido].productos;
                let [totalCuenta, productoExiste]= calcularTotalPagar(productos);
                pedidos[indicePedido].totalPagar=totalCuenta;
                console.log(pedidos[indicePedido]);
                return [200, "modificado"];
            }
        }else return [400,"producto no encontrado"];
    }else return [400,"pedido no encontrado"];
}

const eliminarProductoPedido=(idPedido, idProducto)=>{
    const indicePedido= pedidos.findIndex(ped=>ped.idPedido==idPedido);
    if(indicePedido>-1){
        const indiceProducto= pedidos[indicePedido].productos.findIndex(prod=>prod.idProducto==idProducto);
        if(indiceProducto>-1){
                pedidos[indicePedido].productos.splice(indiceProducto,1);
                const productos= pedidos[indicePedido].productos;
                let [totalCuenta, productoExiste]= calcularTotalPagar(productos);
                pedidos[indicePedido].totalPagar=totalCuenta;
                return [200, "producto eliminado"];
        }else return [400, "producto no encontrado"];
    }else return [400, "pedido no encontrado"];
}

const agregarProductoPedido=(idPedido, productoNuevo)=>{
    const {idProducto, cantidadProducto}= productoNuevo;
    const indicePedido= pedidos.findIndex(ped=>ped.idPedido==idPedido);
    if(indicePedido>-1){
        const indiceProducto= obtenerProductos().findIndex(prod=>prod.idProducto==idProducto);
        if(indiceProducto>-1){
            if(cantidadProducto != ""){
                producto={
                    idProducto: idProducto,
                    cantidadProducto: cantidadProducto
                }
                pedidos[indicePedido].productos.push(producto);
                const productos= pedidos[indicePedido].productos;
                let [totalCuenta, productoExiste]= calcularTotalPagar(productos);
                pedidos[indicePedido].totalPagar=totalCuenta;
                return [200,"Agregado"];
            }else return [400,"cantidad no valida"];
        }else return [400,"producto no encontrado"];
    }else return [400,"pedido no encontrado"];
}


const calcularTotalPagar=(productos)=>{
    let totalCuenta=0;
    let productoExiste=false;
    productos.forEach(ped => {
        let indiceProducto= obtenerProductos().findIndex(p=>p.idProducto === ped.idProducto);
        if(indiceProducto >= 0){
            totalCuenta += obtenerProductos()[indiceProducto].precio*ped.cantidadProducto;
            productoExiste= true;
        } else return [totalCuenta, false];
    });
    return [totalCuenta, productoExiste];
}

const obtenerPedidosUsuario=(usuario)=>{
    const indiceUsuario= pedidos.findIndex(u=>u.usuario.nombreUsuario===usuario);
    if(indiceUsuario>-1){
        pedido={
            "Numero de Pedido" : pedidos[indiceUsuario].idPedido,
            "Productos" : pedidos[indiceUsuario].productos,
            "Total  de la Cuenta" : pedidos[indiceUsuario].totalPagar,
            "Direccion" : pedidos[indiceUsuario].direccion,
            "Medio pago" : pedidos[indiceUsuario].medioPago,
            "Estado pedido" : pedidos[indiceUsuario].estadoPedido,
        }
        return pedido;
    }else return "no hay pedidos"
    
}

const cambiarEstado = (idPedido, usuario, estadoPedido)=>{
    const indicePedido= pedidos.findIndex(p=>p.idPedido==idPedido);
    if(indicePedido>-1){
        if(estadoPedido){
            const indiceUsuario= pedidos.findIndex(u=>u.usuario.nombreUsuario===usuario);
            const esAdmin=obtenerUsuarios()[indiceUsuario].esAdmin;
            console.log(estadoPedido);
            if(estadoPedido === "En Preparacion" || estadoPedido ==="Enviado" || estadoPedido==="Entregado"){
                    pedidos[indicePedido].estadoPedido=estadoPedido;
                    return [200, "estado modificado"];
            } else return [400,"Estado no valido, estado debe ser: En Preparacion, Enviado o Entregado"];
        } else return [400,"estado vacio "];
    }else return [400,"pedido no encontrado"];
}

const cambiarEstadoUsuario = (idPedido, usuario)=>{
    const indicePedido= pedidos.findIndex(p=>p.idPedido==idPedido);
    const pedidosFiltro=pedidos.filter(ped=>ped.usuario.nombreUsuario===usuario);
    const accesoPedido= pedidosFiltro.find(ped=>ped.idPedido==idPedido);
    if(accesoPedido){
        pedidos[indicePedido].estadoPedido="Confirmado";
        return [200, "Pedido confirmado"];
    }else return [400, "El usuario no acceso al pedido o pedido no encontrado"];    
}

module.exports={obtenerPedidos, agregarPedido, modificarContidadProducto, eliminarProductoPedido, agregarProductoPedido, 
                obtenerPedidosUsuario, cambiarEstado, cambiarEstadoUsuario}