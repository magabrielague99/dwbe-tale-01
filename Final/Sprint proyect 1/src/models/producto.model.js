const productos =[{
    idProducto: 1,
    nombreProducto: "Hamburguesas",
    precio: 8000,
},
{
    idProducto: 2,
    nombreProducto: "Hot Dog",
    precio: 5000,
},
{
    idProducto: 3,
    nombreProducto: "Coca-cola",
    precio: 1500
}
]

const obtenerProductos=()=>{
    return productos;
}

const agregarProducto = (productoNuevo) => {
    const {nombreProducto, precio}= productoNuevo;
    if(nombreProducto==""|| precio==(0 || "")) return [400, "Campos vacios"];
    else{
        const productoDuplicado= productos.find(u=>u.nombreProducto===nombreProducto);
        if(productoDuplicado) return [400, "El producto ya esta registrado"];
        else{
            if(Number.parseFloat(precio)){
                producto={
                    id: productos[productos.length-1].id+1,
                    nombreProducto: nombreProducto,
                    precio: precio
                }
                productos.push(producto);
                return [201, "Producto creado"];
            }
            else return [400, "El formato del precio no es valido"];
        }  
    }        
}

const modificarProducto=(idProducto, productoNuevo)=>{
    const {nombreProducto, precio} = productoNuevo;
    if(Number.parseInt(idProducto)){
        const indiceProducto= productos.findIndex(p=>p.idProducto===Number.parseInt(idProducto));
        if(nombreProducto.length>0){
            const productoDuplicado= productos.find(u=>u.nombreProducto===nombreProducto);
            if(productoDuplicado) return [400, "Nombre del producto ya registrado"];
            else productos[indiceProducto].nombreProducto=nombreProducto;
        } 
        if(Number.parseFloat(precio)){
            productos[indiceProducto].precio=precio;
        }
        return [200, "Producto modifcado"]
    }else return[400, "Formato id no valido"]
}

const eliminarProducto=(idProducto)=>{
    if(Number.isInteger(idProducto)){
        const indiceProducto=productos.findIndex(p=>p.idProducto === Number.parseInt(idProducto));
        if(indiceProducto===-1) return [400, "Producto no encontrado "];
        productos.splice(indiceProducto,1);
        console.log(productos);
        return [200, "Producto eliminado"];
    } else return [400, "Formato de Id no valido "]
}

module.exports={agregarProducto, modificarProducto, obtenerProductos, eliminarProducto}