const usuarios = [
    {
        id: 1,
        nombreUsuario: "ggmaria",
        nombreCompleto: "Gabriela Guerrero",
        email:"gabrielaguerrero57@hotmail.com",
        telefono: 3186090430,
        direccion: "calle 27 N° 7-58",
        contrasenia: "12345",
        esAdmin: true
    },
    {
        id: 2,
        nombreUsuario: "pepe2",
        nombreCompleto: "Pedro perez",
        email:"pepito@hotmail.com",
        telefono: 3167587865,
        direccion: "calle 30 N° 8-50",
        contrasenia: "1234A",
        esAdmin: false
    }
];


const agregarUsuario = (usuarioNuevo) => {
    const {nombreUsuario, nombreCompleto, email, telefono, direccion, contrasenia}= usuarioNuevo;
    if(nombreUsuario=="" || nombreCompleto=="" || email==""|| telefono==(0 ||"") || direccion==""|| contrasenia=="") return [400, "CAmpos vacios"];
    else{
        if(email.search(/\w+@\w+/) >= 0 && pass.length > 6){
            const emailDuplicado= usuarios.find(u=>u.email===email);
            const usuarioDuplicado= usuarios.find(u=>u.nombreUsuario===nombreUsuario);
            if(emailDuplicado || usuarioDuplicado) return [400, "Usuario y/o email ya registrado"];
            else{
                usuario={
                    id: usuarios[usuarios.length-1].id+1,
                    nombreUsuario: nombreUsuario,
                    nombreCompleto: nombreCompleto,
                    email: email,
                    telefono: telefono,
                    direccion: direccion,
                    contrasenia: contrasenia,
                    esAdmin: false, 
                }
                usuarios.push(usuario);
                return [201, "Usuario Creado"];
            }
        } else return[400, "email no valido y/o contraseña menor a 6 digitos"]
    }    
}

const obtenerUsuarios=()=>{
    return usuarios;
}

const IniciarSesion=(usuario)=>{
    const {nombreUsuario, contrasenia}= usuario;
    const usuarioEncontrado = obtenerUsuarios().find(u => u.nombreUsuario === nombreUsuario && u.contrasenia === contrasenia);
    if (usuarioEncontrado) return [200, "Ususario Logeado"];
    else return [401, "Usuario y/o contraseña incorrectos"];
}

module.exports = {agregarUsuario, obtenerUsuarios, IniciarSesion}
